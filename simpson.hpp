#ifndef SIMPSON_HPP
#define SIMPSON_HPP

template <typename F>
double integrate(const double a , const double b , const unsigned int bins, F& f){


	double step_size = (b-a)/(2*bins);
	double I = 0;
	for (unsigned int i=1; i<bins; ++i){
		I+= 4*f(step_size*2);
		I+= 2*f(step_size*2+1);	
	}
	I += f(a) + f(b);
	return I;
} 

	

 

#endif 
