#include "simpson.hpp"
#include <iostream>
#include <cmath>

int main(){


	double a = 0;
	double b = M_PI;

	unsigned nbins = 20;
	auto f = [](double x){ return std::sin(x);};


	std::cout << "Integral of sin(x): x=0 to b=M_PI:	": << integrate(a,b,nbins,f);

	
	return 0;
}



